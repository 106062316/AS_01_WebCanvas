window.onload = () => {
    var isDrawing = false;
    var currentTool = "";
    var pos_x = 0;
    var pos_y = 0;
    var currentBrushSize = 4;
    var currentColor = "#000000";
    var undoStack = [];
    var redoStack = [];
    const myCanvas = document.getElementById("jspaint");
    const ctx = myCanvas.getContext("2d");
    var w = myCanvas.width;
    var h = myCanvas.height;
    ctx.lineCap = "round";

    document.getElementById("brush").addEventListener("click", () => {
        currentTool = "brush";
        changeCursor(currentTool);
    });

    document.getElementById("eraser").addEventListener("click", () => {
        currentTool = "eraser";
        changeCursor(currentTool);
    });

    document.getElementById("circle").addEventListener("click", () => {
        currentTool = "circle";
        changeCursor(currentTool);
    });

    document.getElementById("oval").addEventListener("click", () => {
        currentTool = "oval";
        changeCursor(currentTool);
    });

    document.getElementById("rectangle").addEventListener("click", () => {
        currentTool = "rectangle";
        changeCursor(currentTool);
    });

    document.getElementById("triangle").addEventListener("click", () => {
        currentTool = "triangle";
        changeCursor(currentTool);
    });

    document.getElementById("fill").addEventListener("click", () => {
        changeCursor(currentTool);
        changeIcon();
    });

    document.getElementById("drawtext").addEventListener("click", () => {
        currentTool = "text";
        changeCursor(currentTool);
    });

    document.getElementById("reset").addEventListener("click", () => {
        var prompt = confirm("Are you sure you want to clear the canvas?")
        if (prompt) ctx.clearRect(0, 0, w, h);
    });

    document.getElementById("undo").addEventListener("click", () => {
        if (undoStack.length > 0) {
            redoStack.push(ctx.getImageData(0, 0, w, h));
            ctx.putImageData(undoStack.pop(), 0, 0);
        }
    });

    document.getElementById("redo").addEventListener("click", () => {
        if (redoStack.length > 0) {
            undoStack.push(ctx.getImageData(0, 0, w, h));
            ctx.putImageData(redoStack.pop(), 0, 0);
        }
    });

    document.getElementById("download").addEventListener("click", () => {
        var link = document.createElement('a');
        var DataURL = myCanvas.toDataURL();
        link.download = document.getElementById("filename").value + ".png";
        link.href = DataURL;
        link.click();
    });

    myCanvas.addEventListener("mousedown", (cur) => {
        isDrawing = true;
        undoStack.push(ctx.getImageData(0, 0, w, h));
        currentBrushSize = document.getElementById("sizeofbrush").value;
        ctx.lineWidth = currentBrushSize;
        currentColor = document.getElementById("selectColor").value;
        ctx.strokeStyle = currentColor;
        ctx.fillStyle = currentColor;
        pos_x = cur.offsetX;
        pos_y = cur.offsetY;
        ctx.globalCompositeOperation = "source-over";
        if (currentTool === "brush" || currentTool === "eraser") {
            if (currentTool === "eraser") {
                ctx.globalCompositeOperation = "destination-out";
            }
            ctx.beginPath();
            ctx.moveTo(cur.offsetX, cur.offsetY);
            ctx.lineTo(cur.offsetX, cur.offsetY);
            ctx.stroke();
        } else if (currentTool === "text") {
            txt = document.getElementById("font");
            ctx.font = document.getElementById("fontSize").value + "px " + txt.options[txt.selectedIndex].value;
            ctx.fillText(document.getElementById("textinput").value, cur.offsetX, cur.offsetY);
        }
    });

    myCanvas.addEventListener("mousemove", (cur) => {
        var pre, Checked = document.getElementById("fill").checked;
        if (isDrawing) {
            if (currentTool === "brush" || currentTool === "eraser") {
                ctx.lineTo(cur.offsetX, cur.offsetY);
                ctx.stroke();
                ctx.beginPath();
                ctx.moveTo(cur.offsetX, cur.offsetY);
            } else if (currentTool === "circle") {
                ctx.putImageData(pre = undoStack.pop(), 0, 0);
                undoStack.push(pre);
                ctx.beginPath();
                ctx.arc(pos_x, pos_y, Math.sqrt(Math.pow(cur.offsetX - pos_x, 2) + Math.pow(cur.offsetY - pos_y, 2)), 0, 2 * Math.PI);
                if (Checked) {
                    ctx.fill();
                } else {
                    ctx.stroke();
                }
            } else if (currentTool === "oval") {
                ctx.putImageData(pre = undoStack.pop(), 0, 0);
                undoStack.push(pre);
                ctx.beginPath();
                ctx.ellipse(pos_x, pos_y, Math.abs(cur.offsetX - pos_x), Math.abs(cur.offsetY - pos_y), 0, 0, 2 * Math.PI);
                if (Checked) {
                    ctx.fill();
                } else {
                    ctx.stroke();
                }
            } else if (currentTool === "rectangle") {
                ctx.putImageData(pre = undoStack.pop(), 0, 0);
                undoStack.push(pre);
                if (Checked) {
                    ctx.fillRect(pos_x, pos_y, cur.offsetX - pos_x, cur.offsetY - pos_y);
                } else {
                    ctx.strokeRect(pos_x, pos_y, cur.offsetX - pos_x, cur.offsetY - pos_y);
                }
            } else if (currentTool === "triangle") {
                ctx.putImageData(pre = undoStack.pop(), 0, 0);
                undoStack.push(pre);
                ctx.beginPath();
                ctx.moveTo(pos_x, pos_y);
                ctx.lineTo(cur.offsetX, cur.offsetY);
                ctx.lineTo(2 * pos_x - cur.offsetX, cur.offsetY);
                ctx.closePath();
                if (Checked) {
                    ctx.fill();
                } else {
                    ctx.stroke();
                }
            }
        }
    });

    window.addEventListener("mouseup", () => {
        isDrawing = false;
    });
}

function changeIcon() {
    var Checked = document.getElementById("fill").checked;
    if (Checked) {
        document.getElementById("circle").src = "img/circle-full.png"
        document.getElementById("oval").src = "img/ellipse.png";
        document.getElementById("rectangle").src = "img/square-full.png";
        document.getElementById("triangle").src = "img/triangle-full.png";
    } else {
        document.getElementById("circle").src = "img/circle.png"
        document.getElementById("oval").src = "img/oval.png";
        document.getElementById("rectangle").src = "img/square.png";
        document.getElementById("triangle").src = "img/triangle.png";
    }
}

function changeCursor(tool) {
    const myCanvas = document.getElementById("jspaint");
    var Checked = document.getElementById("fill").checked;
    if (tool === "brush") {
        myCanvas.style.cursor = "url(img/brush.png), auto";
    } else if (tool === "eraser") {
        myCanvas.style.cursor = "url(img/eraser.png), auto";
    } else if (tool === "circle") {
        if (Checked) {
            myCanvas.style.cursor = "url(img/circle-full.png), auto";
        } else {
            myCanvas.style.cursor = "url(img/circle.png), auto";
        }
    } else if (tool === "oval") {
        if (Checked) {
            myCanvas.style.cursor = "url(img/ellipse.png), auto";
        } else {
            myCanvas.style.cursor = "url(img/oval.png), auto";
        }
    } else if (tool === "rectangle") {
        if (Checked) {
            myCanvas.style.cursor = "url(img/square-full.png), auto";
        } else {
            myCanvas.style.cursor = "url(img/square.png), auto";
        }
    } else if (tool === "triangle") {
        if (Checked) {
            myCanvas.style.cursor = "url(img/triangle-full.png), auto";
        } else {
            myCanvas.style.cursor = "url(img/triangle.png), auto";
        }
    } else if (tool === "text") {
        myCanvas.style.cursor = "url(img/font.png), auto";
    }
}

function uploadImage() {
    const myCanvas = document.getElementById("jspaint");
    const ctx = myCanvas.getContext("2d");

    if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
        alert('The File APIs are not fully supported in this browser.');
        return;
    }

    input = document.getElementById('upload');

    if (!input) {
        alert("Um, couldn't find the upload element.");
    } else if (!input.files) {
        alert("This browser doesn't seem to support the `files` property of file inputs.");
    } else {
        var fr = new FileReader();
        fr.onload = () => {
            var img = new Image();
            img.onload = () => ctx.drawImage(img, 0, 0, myCanvas.width, myCanvas.height);
            img.src = fr.result;
        }
        fr.readAsDataURL(input.files[0]);
    }

    undoStack.push(ctx.getImageData(0, 0, myCanvas.width, myCanvas.height));
}