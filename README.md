# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
![](https://i.imgur.com/gXBFSAF.jpg)

    The button on the left labeled "Color" is used to select the color for 
    brush and shapes, and the "size" element controls the brush size, 
    it also affects eraser size and the line width of shapes.
    
    The icons from left to right represents brush, eraser, circle, oval (ellipse),
    rectangle, triangle, text, reset, undo, redo, download and upload.
    
    The checkbox labeled "Fill" is used to decide whether the shape drawn is filled.
    
    The text drawer works like a stamp. You have to first type some text in the box
    to the right of 'A'. When you finish typing, simply click on the 'A' and you
    can use it like a stamp to paste your text anywhere on the canvas.
    
    You can also control the font size and font family used.
    
    The download icon, when clicked, will download a image with a default name 
    "MyCanvas.png" to your default download directory. You can change the name 
    of image by modifying the text in the box to the right of the download icon.
    
    The upload icon is complementary to the file input to the right, 
    clicking on that will have no effect. Please use the "Choose File" button.
    
### Gitlab page link
[106062316_AS_01_WebCanvas](https://106062316.gitlab.io/AS_01_WebCanvas)

<style>
table th {
    width: 100%;
}
</style>